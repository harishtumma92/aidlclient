package com.example.aidlclient

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.os.RemoteException
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.aidlserver.IRemoteService
import com.example.aidlserver.IRemoteServiceCallback
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import java.lang.Exception


class MainActivity : AppCompatActivity() {
    lateinit var serviceConnection: ServiceConnection
    lateinit var iRemoteService:IRemoteService
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mCallbcak: IRemoteServiceCallback = object : IRemoteServiceCallback.Stub() {
            override fun sensorOnChanged(vlaue: FloatArray) {
                //Timber.d("unRegister callback:")
                try {
                    text_x.setText(vlaue[0].toString())
                    text_y.setText(vlaue[1].toString())
                    text_z.setText(vlaue[2].toString())
                }catch (e:Exception)
                {
                    Timber.e(e.toString())
                }
                //text_x.setText("${vlaue[0].toString()}, ${vlaue[1].toString()} , ${vlaue[2].toString()}")
            }
        }

        serviceConnection=object :ServiceConnection{
            override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
                Timber.i("on Service Connected called")
                if (p1 != null) {
                    iRemoteService = IRemoteService.Stub.asInterface(p1)
                    try {
                        var flag=iRemoteService.registerCallback(mCallbcak)
                        Timber.i("RemoteService callback register:$flag")
                        val message: String = iRemoteService.getMessage()
                        Toast.makeText(this@MainActivity, message, Toast.LENGTH_LONG).show()
                    } catch (e: RemoteException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                        Timber.e(e.printStackTrace().toString())
                    }
                }
            }

            override fun onServiceDisconnected(p0: ComponentName?) {
                Timber.i("on Service Disconnected called")
            }

        }
        bind.setOnClickListener {
            bindsevice()
        }
        start.setOnClickListener {
            bindsevice()
        }

    }

    private fun bindsevice()
    {
        Timber.i("bindservice called")
        var intetn= Intent("connect_aidl_rotation_sensor")
        intetn.setPackage("com.example.aidlserver")
        bindService(intetn, serviceConnection as ServiceConnection, BIND_AUTO_CREATE)

    }
}