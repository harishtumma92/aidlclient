// IRemoteServiceCallback.aidl
package com.example.aidlserver;

// Declare any non-default types here with import statements
interface IRemoteServiceCallback {
    void sensorOnChanged(in float[] vlaue);
}

